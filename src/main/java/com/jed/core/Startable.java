package com.jed.core;

/**
 * Created by Peter Colapietro on 10/31/14.
 */
public interface Startable {

    /**
     * 
     */
    void start();

}
