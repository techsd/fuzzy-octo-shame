# fuzzy-octo-shame
### 2D Platformer skeleton built with LWJGL and Slick2D

#### Useful stuff:

* `mvn nativedependencies:copy`

* `-Djava.library.path=/path/to/project/root/target/natives`
        
* `-Dorg.slf4j.simpleLogger.defaultLogLevel=debug`

#### "How to configure slf4j-simple"
* http://stackoverflow.com/a/14545138
